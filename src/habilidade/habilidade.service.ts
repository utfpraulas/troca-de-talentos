import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Habilidade } from './habilidade.entity';


@Injectable()
export class HabilidadesService {
  constructor(
    @InjectRepository(Habilidade)
    private readonly repo: Repository<Habilidade>,
  ) { }

  async createAndupdate(id: string, form): Promise<Habilidade> {
    console.log(form)
    if (id) {//update
      const habilidade = await this.findOne(id);
      return this.repo.save({
        ...habilidade,
        ...form
      });
    } else {//create     
      return this.repo.save({
        ...form
      });
    }
  }

  async findAll(): Promise<Habilidade[]> {
    return this.repo.find({ order: { name: 'ASC' } });
  }

  findOne(id: string): Promise<Habilidade> {
    return this.repo.findOne(id);
  }
  findOneByUser(id: number): Promise<Habilidade> {
    return this.repo.findOne({ iduser: id });
  }

  findOneByName(id: string): Promise<Habilidade> {
    return this.repo.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.repo.delete(id);
  }
}
