import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HabilidadeController } from './habilidade.controller';
import { Habilidade } from './habilidade.entity';
import { HabilidadesService } from './habilidade.service';


@Module({
  imports: [TypeOrmModule.forFeature([Habilidade]),],
  providers: [HabilidadesService],
  controllers: [HabilidadeController],
})
export class HabilidadesModule {}
