import { Body, Controller, Param, Post, } from "@nestjs/common";
import { HabilidadesService } from "./habilidade.service";

@Controller('habilidade')
export class HabilidadeController {
  constructor(
    private readonly service: HabilidadesService,
  ) { }

  @Post('find/hability/user/:id')
  async byuser(@Param("id") user): Promise<any> {
    return this.service.findOneByUser(user)
  }

  @Post('find/hability/name/:name')
  async byname(@Param("name") user): Promise<any> {
    return this.service.findOneByUser(user)
  }
}