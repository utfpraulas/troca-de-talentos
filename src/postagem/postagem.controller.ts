import { Body, Controller, Param, Post, } from "@nestjs/common";
import { PostagemsService } from "./postagem.service";

@Controller('postagem')
export class PostagemController {
  constructor(
    private readonly service: PostagemsService,
  ) { }

  @Post('find/postage/user/:id')
  async byuser(@Param("id") user): Promise<any> {
    return this.service.findOneByUser(user)
  }

  @Post('find/postage/name/:name')
  async byname(@Param("name") user): Promise<any> {
    return this.service.findOneByUser(user)
  }
}
