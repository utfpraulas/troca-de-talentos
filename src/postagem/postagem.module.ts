import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostagemController } from './postagem.controller';
import { Postagem } from './postagem.entity';
import { PostagemsService } from './postagem.service';


@Module({
  imports: [TypeOrmModule.forFeature([Postagem]),],
  providers: [PostagemsService],
  controllers: [PostagemController],
})
export class PostagemsModule {}
