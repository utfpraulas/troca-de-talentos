import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class Postagem {
  @PrimaryGeneratedColumn("uuid")
  id: number;

  @Column({nullable: true})
  name: string;

  @Column({nullable: true})
  iduser: number;

  @Column({nullable: true})
  descricao: string;
}
