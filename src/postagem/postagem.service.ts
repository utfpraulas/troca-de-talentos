import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Postagem } from './postagem.entity';


@Injectable()
export class PostagemsService {
  constructor(
    @InjectRepository(Postagem)
    private readonly repo: Repository<Postagem>,
  ) { }

  async createAndupdate(id: string, form): Promise<Postagem> {
    console.log(form)
    if (id) {//update
      const postagem = await this.findOne(id);
      return this.repo.save({
        ...postagem,
        ...form
      });
    } else {//create     
      return this.repo.save({
        ...form
      });
    }
  }

  async findAll(): Promise<Postagem[]> {
    return this.repo.find({ order: { name: 'ASC' } });
  }

  findOne(id: string): Promise<Postagem> {
    return this.repo.findOne(id);
  }
  findOneByUser(id: number): Promise<Postagem> {
    return this.repo.findOne({ iduser: id });
  }

  findOneByName(id: string): Promise<Postagem> {
    return this.repo.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.repo.delete(id);
  }
}
