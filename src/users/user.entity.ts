import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class User {
  @PrimaryGeneratedColumn("uuid")
  id: number;

  @Column({nullable: true})
  name: string;

  @Column({nullable: true})
  campus: string;

  @Column({nullable: true})
  curso: string;
  
  @Column({nullable: true})
  email: string;

  @Column({ default: true })
  active: boolean;

  @Column({ nullable: true })
  admin: boolean;

  @Column({nullable: true})
  pass: string;
  
}
