import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';


@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly repo: Repository<User>,
  ) { }

  async createAndupdate(id: number, form): Promise<User> {
    console.log(form)
    if (id) {//update
      const user = await this.findOne(id);
      return this.repo.save({
        ...user,
        ...form
      });
    } else {//create     
      return this.repo.save({
        ...form
      });
    }
  }

  async findAll(): Promise<User[]> {
    return this.repo.find({ order: { name: 'ASC' } });
  }

  async findOne(id: number): Promise<User> {
    return this.repo.findOne(id);
  }

  async remove(id: number): Promise<void> {
    await this.repo.delete(id);
  }

  async login(dados): Promise<any> {
    let user = await this.repo.findOne({ name: dados.name });
    if (!user) {
      user = await this.repo.findOne({ email: dados.name });
    }
    if (user && user.pass == dados.pass) {
      return 'gerar token'
    } else {
      return -1
    }
  }
}
