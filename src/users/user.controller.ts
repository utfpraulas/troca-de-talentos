import { Body, Controller, Post, } from "@nestjs/common";
import { UsersService } from "./user.service";

@Controller('user')
export class UserController {
  constructor(
    private readonly service: UsersService,
  ) { }

  @Post('login')
  async login(@Body() dados): Promise<string> {
    //chama a validaçao
    const token = await this.service.login(dados)
    return token;
  }

  @Post('find/profile/user/:id')
  async profile(@Body() dados): Promise<any> {
    //chama a validaçao
    const perfil = await this.service.findOne(dados.id)
    return perfil;
  }
}
